<?php
class GalleryDB {
    private $dbh;
    private static $PDOInstance;

    private function __construct($host, $dbname, $user, $pwd) {
        try {
            $this->dbh = new PDO("mysql:host=$host; dbname=$dbname", $user, $pwd);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    public static function init($host, $dbname, $user, $pwd = '') {
        if (!self::$PDOInstance) {
            self::$PDOInstance = new GalleryDB($host, $dbname, $user, $pwd);
        }
        return self::$PDOInstance;
    }

    public function checkLogin($email, $pwd) {

        $st = $this->dbh->prepare("SELECT * FROM user WHERE email = :email AND password = :pwd");
        $st->bindValue(':email', $email);
        $st->bindValue(':pwd', $pwd);
        $st->execute();
//        $st = $this->dbh->query("SELECT * FROM user WHERE email = '$email' AND password = '$pwd'");
        $user = $st->fetchAll();
        return $user;
    }

    public function createAlbum($userID, $name, $desc) {
//        $st = $this->dbh->query("INSERT INTO album(name, description, user_id) VALUES('$name', '$desc', '$userID')");
        $st = $this->dbh->prepare("INSERT INTO album(name, description, user_id) VALUES(:name, :desc, :userID)");
        $st->bindValue(':name', $name);
        $st->bindValue(':desc', $desc);
        $st->bindValue(':userID', $userID, PDO::PARAM_INT);
        if (!$st->execute()) {
            echo "Ошибка при вставке в БД";
            var_dump($st->errorInfo());
        }
    }
}

