<?php

$config = require 'config.php';
require_once 'GalleryDB.php';
require_once 'tpl/album.create.php';

if (isset($_POST['name']) && isset($_POST['description'])) {
    if (isset($_SESSION['user'])) {
        $db = GalleryDB::init('localhost', 'photo_gallery22', 'root');
        $name = $_POST['name'];
        $desc = $_POST['description'];
        $userID = $_SESSION['user'][0]['id'];
        $db->createAlbum($userID, $name, $desc);
}
}