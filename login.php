<?php

$config = require 'config.php';
require_once 'GalleryDB.php';
require_once 'tpl/login.php';

if (isset($_POST['email']) && isset($_POST['password'])) {
    $userEmail = $_POST['email'];
    $pwd = sha1($_POST['password']);
    $db = GalleryDB::init('127.0.0.1', 'photo_gallery22', 'root');
    $user = $db->checkLogin($userEmail, $pwd);
    if ($user) {
        $_SESSION['auth'] = true;
        $_SESSION['user'] = $user;
        Header('Location: index.php');
    }
}