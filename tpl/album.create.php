<?php


require_once 'parts/header.php';

?>

    <div class="col-lg-6 col-lg-offset-3">
        <form method="post">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" />
                <br/>
                <label>Description</label>
                <input type="text" name="description" class="form-control" />
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary form-control" />
            </div>
        </form>
    </div>


<?php

require_once 'parts/footer.php';

?>