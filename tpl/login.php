<?php

require_once 'parts/header.php';
?>

<div class="col-lg-6 col-lg-offset-3 ng-scope">


    <div class="panel panel-success" style="margin-top:20px;">
        <div class="panel-heading">
            <h2 style="margin:0;" class="ng-binding">Login</h2>
        </div>
        <div class="panel-body">


            <form method="POST" ng-submit="loginForm.submit()" novalidate="" name="loginFrm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required">
                <div class="form-group required ">
                    <label class="control-label ng-binding" for="loginform-email">Email</label>
                    <input type="email" id="loginform-email" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" name="email" ng-model="loginForm.email" ng-required="true" required="required">
                </div>

                <div class="form-group required ">
                    <label class="control-label ng-binding" for="loginform-password">Password</label>
                    <input type="password" id="loginform-password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" name="password" ng-model="loginForm.password" ng-required="true" required="required">
                </div>

                <div class="form-group">
                    <input type="submit" id="login-button" class="btn btn-primary form-control" value="Login">
                </div>
            </form>

        </div>
    </div>
</div>

<?php

require_once 'parts/footer.php';

?>