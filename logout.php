<?php

$config = require 'config.php';
require_once 'GalleryDB.php';

if (isset($_SESSION['auth'])) {
    unset($_SESSION['auth'], $_SESSION['user']);
        Header('Location: index.php');
}